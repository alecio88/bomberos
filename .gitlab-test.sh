#!/bin/sh

# Ensure that php is working
php -v

# Ensure that mysql server is up and running
ping -c 3 postgres

# Run unit testing with PHPUnit
php vendor/bin/phpunit --coverage-text --colors=never
